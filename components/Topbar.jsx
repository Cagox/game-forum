import React from 'react'
import styles from '../styles/components/topbar.module.scss'

export default function componentName() {
  return (
    <div className={styles.topbar}>
      <div className={styles.inner}>
        <img src="/icons/group-3.svg" className={styles.profileImage} />
        <div className={styles.searchDiv}>
          <input type="text" placeholder="Search games, gears, accessories.." />
          <img src="/icons/icn-search.svg" className={styles.searchIcon} />
        </div>
        <img src="/icons/icn-profile.svg" className={styles.profileIcon} />
        <img src="/icons/icn-save.svg" className={styles.saveIcon} />

        <div className={styles.brace}></div>

        <img src="/icons/shape.svg" className={styles.shapeIcon}></img>
        <img src="/icons/icn-save.svg" className={styles.loveIcon} />
        <img src="/icons/icn-shopping.svg" className={styles.shoppingIcon} />
      </div>

      <div className={styles.tabs}>
        <div className={styles.tab1}>
          <img src="/icons/icn-home.svg" className={styles.homeIcon} />
          Home
        </div>

        <div className={styles.tab2}>
          <img src="/icons/icn-games.svg" className={styles.gamesIcon} />
          Social
        </div>

        <div className={styles.tab3}>
          <img src="/icons/icn-market.svg" className={styles.marketIcon} />
          Market
        </div>

        <div className={styles.tab4}>
          <img
            src="/icons/icn-tournaments.svg"
            className={styles.tournamentsIcon}
          />
          Tournaments
        </div>
      </div>
    </div>
  )
}
