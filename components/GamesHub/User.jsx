import React from 'react'
import styles from '../../styles/components/user.module.scss'

export default function User({ image, username, followers }) {
  return (
    <div className={styles.userComponent}>
      <div className={styles.left}>
        <img className={styles.userImage} src={image} />

        <div className={styles.userBody}>
          <div className={styles.username}>{username}</div>
          <div className={styles.userFollowers}>{followers} Followers</div>
        </div>
      </div>

      <img className={styles.addUserButton} src="/icons/addUser.svg" />
    </div>
  )
}
