import React from 'react'
import cls from 'classnames'
import styles from '../../styles/components/comment.module.scss'

export default function componentName({
  username,
  atUsername,
  userImage,
  followers,
  minutes,
  commentText,
  isClapsed,
  clapses,
  commentNumber,
  isPro,
}) {
  return (
    <div className={styles.comment}>
      <div className={styles.commentHeader}>
        <div className={styles.userInfo}>
          <img className={styles.userImage} src={userImage} />

          <div className={styles.userTexts}>
            <div className={styles.username}>
              {' '}
              {username} {isPro && <img src="/icons/pro-tag-copy-3.svg" />}
            </div>

            <div className={styles.userMore}>
              <div className={styles.atUsername}>@{atUsername}</div>
              <div className={styles.followers}>{followers} Followers</div>
            </div>
          </div>
        </div>
        <div className={styles.hours}>
          {minutes / 60 >= 1
            ? `${Math.floor(minutes / 60)}hrs`
            : `${minutes}mins`}{' '}
          ago
        </div>
      </div>

      <div className={styles.commentText}>
        <pre
          style={{
            fontFamily: 'Rubik-Regular',
            wordBreak: 'break-all',
          }}
        >
          {commentText.slice(0, 182)}
          {commentText.slice(182) && (
            <span>
              ..<span className={styles.readMore}> Read More</span>
            </span>
          )}
        </pre>
      </div>

      <div className={styles.bottomBar}>
        <div className={styles.left}>
          <img src="/icons/repost.svg" />
          <img src="/icons/group-6.svg" className={styles.button2} />
          <img
            src={isClapsed ? '/icons/group-7.svg' : '/icons/notclapsed.svg'}
            className={styles.button3}
          />
          {clapses && <span className={styles.claps}>{clapses}</span>}
        </div>

        <div className={styles.right}>
          <div
            className={cls(
              styles.commentNumber,
              !commentNumber && styles.firstComment
            )}
          >
            {' '}
            {commentNumber ? `${commentNumber} comments` : 'Add your comment'}
          </div>
          <img src="/icons/icn-comments.svg" />
        </div>
      </div>
    </div>
  )
}
