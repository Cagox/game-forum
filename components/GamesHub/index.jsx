import React from 'react'
import styles from '../../styles/components/gameshub.module.scss'

import Comment from './Comment'
import Channel from './Channel'
import User from './User'

export default function GamesHub({ posts, channels, users }) {
  console.log(posts)
  return (
    <div className={styles.gameshub}>
      <div className={styles.title}>
        <div className={styles.text}>
          <div className={styles.header}>Games Hub</div>
          <div className={styles.explanation}>
            The best offers, new games, AAA titles and high-quality video
            games..
          </div>
        </div>
        <button className={styles.discover}>Discover All</button>
      </div>

      <div className={styles.inner}>
        <div className={styles.latest}>
          <div className={styles.tabTitle}>Latest posts</div>

          {posts.map((post) => (
            <Comment
              key={post.postid}
              username={post.username}
              atUsername={post.atUsername}
              userImage={post.userImage}
              followers={post.followers}
              minutes={post.minutes}
              commentText={post.commentText}
              isClapsed={post.isClapsed}
              clapses={post.clapses}
              commentNumber={post.commentNumber}
              isPro={post.isPro}
            />
          ))}
        </div>

        <div className={styles.channels}>
          <div className={styles.tabTitle}>Channels</div>

          <div className={styles.brace} />

          {channels.map((channel) => (
            <Channel
              key={channel.postid}
              image={channel.image}
              tag={channel.tag}
              users={channel.users}
            />
          ))}

          <button className={styles.discoverForTabs}>Find More</button>
        </div>

        <div className={styles.whoto}>
          <div className={styles.tabTitle}>Who to follow?</div>

          <div className={styles.brace} />

          {users.map((user) => (
            <User
              key={user.uid}
              image={user.image}
              username={user.username}
              followers={user.followers}
            />
          ))}

          <button className={styles.discoverForTabs}>Discover All</button>
        </div>
      </div>
    </div>
  )
}
