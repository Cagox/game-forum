import React from 'react'
import styles from '../../styles/components/channel.module.scss'

export default function Channel({ image, tag, users }) {
  return (
    <div className={styles.channel}>
      <img className={styles.channelImage} src={image} />

      <div className={styles.channelBody}>
        <div className={styles.channelTag}>#{tag}</div>
        <div className={styles.channelUsers}>{users} Users</div>
      </div>
    </div>
  )
}
