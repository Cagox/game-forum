import React from 'react'
import Head from 'next/head'
import styles from '../styles/pages/index.module.scss'

import Topbar from '../components/Topbar'
import GamesHub from '../components/GamesHub'

function Home({ channels, posts, users }) {
  return (
    <div className="container">
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Topbar />
      <main className={styles.container}>
        <GamesHub posts={posts} channels={channels} users={users} />
      </main>

      {/* <footer>There is Footer</footer> */}
    </div>
  )
}

Home.getInitialProps = async (ctx) => {
  return {
    channels: [
      { postid: '0', image: '/warcraft.jpg', tag: 'social Hub', users: '195K' },
      { postid: '1', image: '/fortnite.jpg', tag: 'Fortnite', users: '240K' },
      {
        postid: '2',
        image: '/warcraft.jpg',
        tag: 'World of Warcraft',
        users: '195K',
      },
      {
        postid: '3',
        image: '/pubg.jpg',
        tag: 'World of Warcraft',
        users: '220K',
      },
      {
        postid: '4',
        image: '/hearthstone.jpg',
        tag: 'HearthStone',
        users: '175K',
      },
      { postid: '5', image: '/csgo.jpg', tag: 'CS:GO', users: '194K' },
      {
        postid: '6',
        image: '/fortnite.jpg',
        tag: 'Call of Duty',
        users: '175K',
      },
      {
        postid: '7',
        image: '/hearthstone.jpg',
        tag: 'Overwatch',
        users: '200K',
      },
      { postid: '8', image: '/warcraft.jpg', tag: 'Diablo III', users: '175K' },
    ],
    posts: [
      {
        postid: 1,
        username: 'MrPaladin',
        atUsername: 'mrpaladin',
        userImage: '/bitmap.jpg',
        followers: 15,
        minutes: 185,
        commentText: `
Sniper isnt overpowered.
Sniper has some of the lowest damage outputs in the game. 50 damage
per shot is terrible, especially with the fire rate.
Compared to heavy, who has 400+ DPS!.. Read More
tell me how sniper compares.
      `,
        isClapsed: true,
        clapses: 256,
        commentNumber: 15,
        isPro: false,
      },
      {
        postid: 2,
        username: 'LeroyJenkins',
        atUsername: 'leroyjenkins',
        userImage: '/user-img-02.jpg',
        followers: 3253,
        minutes: 15,
        commentText: `
It'll still be long enough that it cannot be spammed. But will increase
usefulness of the watch by not.. 
      `,
        isClapsed: false,
        clapses: false,
        commentNumber: 0,
        isPro: true,
      },
      {
        postid: 3,
        username: 'ONYXSnake1223',
        atUsername: 'ONYXSnake1223',
        userImage: '/userimage3.jpg',
        followers: 125,
        minutes: 15,
        commentText: `
If you've not yet played CS:GO, do yourself a favor and play first. Excellent
game, and this one ties in heavily with the story from the last..  
      `,
        isClapsed: false,
        clapses: 1200,
        commentNumber: 156,
        isPro: true,
      },
    ],
    users: [
      {
        uid: 1,
        username: 'XMegatronX',
        followers: '325.860',
        image: '/userimage3.jpg',
      },
      {
        uid: 2,
        username: 'Rikimarue',
        followers: '285.020',
        image: '/userimage4.jpg',
      },
      {
        uid: 3,
        username: 'Wolfie',
        followers: '483.642',
        image: '/userimage5.jpg',
      },
      {
        uid: 4,
        username: 'Thanon',
        followers: '312.245',
        image: '/userimage3.jpg',
      },
      {
        uid: 2,
        username: 'Rikimarue',
        followers: '285.020',
        image: '/userimage4.jpg',
      },
      {
        uid: 5,
        username: 'MrPaladin',
        followers: '433.642',
        image: '/userimage6.jpg',
      },
      {
        uid: 6,
        username: 'Supreme',
        followers: '312.245',
        image: '/userimage5.jpg',
      },
      {
        uid: 7,
        username: 'Murmur',
        followers: '312.245',
        image: '/userimage6.jpg',
      },
      {
        uid: 2,
        username: 'Loremiosum',
        followers: '312.245',
        image: '/bitmap.jpg',
      },
    ],
  }
}

export default Home
